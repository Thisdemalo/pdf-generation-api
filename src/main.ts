import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from './log/logger';
import { ConfigurationService } from './config/configuration.service';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        cors: true,
        logger: Logger.initializeLoggerService({file: true})
    });

    const config: ConfigurationService = app.get(ConfigurationService);
    app.useLogger(Logger.initializeLoggerService(config.getLogConfiguration()))

    //enable CORS for all domains
    app.enableCors({
        origin: "*",
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE, OPTIONS',
        preflightContinue: false,
        optionsSuccessStatus: 204,
    });
    await app.listen(3000);
}

bootstrap();
