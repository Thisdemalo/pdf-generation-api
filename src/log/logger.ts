import * as os from 'os';
import * as winston from 'winston';
import 'winston-daily-rotate-file';
import { WinstonModule } from 'nest-winston';
import { LoggerService } from '@nestjs/common';
import { LogConfiguration, LogFileConfiguration } from '../config/configuration';

export class Logger {

    public static readonly DEFAULT_LEVEL: string = 'info';
    public static readonly DEFAULT_FILE_NAME: string = 'logs/pdf-generation-api-%DATE%.log';
    public static readonly DEFAULT_FILE_DATE_PATTERN: string = 'YYYYMMDD';
    public static readonly DEFAULT_FILE_MAX_SIZE: string = '10m';
    public static readonly DEFAULT_FILE_MAX_FILES: string = '60d';

    public static initializeLoggerService(logConfiguration: LogConfiguration | undefined): LoggerService {
        logConfiguration = logConfiguration ? logConfiguration : {};
        const transports: any[] = [];

        if (logConfiguration.file) {
            const fileConf: LogFileConfiguration = logConfiguration.file === true
                ? {}
                : logConfiguration.file;

            transports.push(new winston.transports.DailyRotateFile({
                filename: fileConf.name ?? Logger.DEFAULT_FILE_NAME,
                datePattern: fileConf.datePatternForFileName ?? Logger.DEFAULT_FILE_DATE_PATTERN,
                zippedArchive: true,
                maxSize: fileConf.maxSize ?? Logger.DEFAULT_FILE_MAX_SIZE,
                maxFiles: fileConf.maxFiles ?? Logger.DEFAULT_FILE_MAX_FILES
            }));
        }

        return WinstonModule.createLogger({
            level: logConfiguration.level ?? Logger.DEFAULT_LEVEL,
            handleExceptions: true,
            exitOnError: false,
            format: winston.format.combine(
                winston.format.errors({stack: true}),
                winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSSZZ'}),
                winston.format.printf(
                    ({level, message, timestamp, stack, ...meta}) => {
                        return `${timestamp} ${level.toUpperCase()}: ${message} ${Object.keys(meta).length !== 0 ? JSON.stringify(meta) : ''} ${stack ? os.EOL + stack : ''}`;
                    }
                )
            ),
            transports: transports.concat(new winston.transports.Console())
        });
    }
}