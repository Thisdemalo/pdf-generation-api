import { Body, Controller, HttpException, HttpStatus, Post, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from "@nestjs/platform-express";
import * as path from 'path';
import { Stream } from "stream";
import { GeneratePdfDto } from './generate-pdf-dto';
import { PdfPuppeteerService } from '../service/puppeteer/pdf-puppeteer.service';

const SUPPORTED_EXTENSIONS = ['.html', '.htm', '.xhtml'];

@Controller('pdf')
export class PdfController {
    constructor(
        private readonly pdfPuppeteerService: PdfPuppeteerService,
    ) {
    }

    @Post()
    @UseInterceptors(FileInterceptor('template'))
    public async generatePdf(@UploadedFile() template: Express.Multer.File,
                             @Body() generatePdfDto: GeneratePdfDto,
                             @Res() response) {
        if (!template || false == this.isSupportedExtension(path.extname(template.originalname))) {
            throw new HttpException({
                status: HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                error: `File ${template?.originalname} doesn't have a supported extension (available extensions: ${SUPPORTED_EXTENSIONS.join()})`
            }, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
        //get content of html file received thanks to buffer
        await this.pdfPuppeteerService.generate(template.buffer.toString())
            .then((pdfStream: Stream) => {
                response.set('Content-Type', 'application/pdf');
                response.set('Content-Disposition', `attachment; filename=result.pdf`);
                //write pdf to body
                pdfStream.pipe(response);
            })
            .catch(error => {
                throw new HttpException({
                    status: HttpStatus.INTERNAL_SERVER_ERROR,
                    error: error.message
                }, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    private isSupportedExtension(extensionName: string) {
        return SUPPORTED_EXTENSIONS.includes(extensionName);
    }
}

