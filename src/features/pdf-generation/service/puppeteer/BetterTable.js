class BetterTable extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.mapOfOriginalTables = new Map();
        this.splittedTablesProcessed = [];
    }

    renderNode(node, sourceNode) {
        if(node.nodeName === "TR"){
            let table = node.offsetParent;
            //is original table
            if(table.dataset.splitFrom === undefined && this.mapOfOriginalTables.has(table.dataset.ref) === false){
                this.mapOfOriginalTables.set(table.dataset.ref, table);
            }
            //is splitted table
            if(table.dataset.splitFrom && this.splittedTablesProcessed.includes(table) === false){
                this.splittedTablesProcessed.push(table);
                const originalRef = table.dataset.splitFrom;
                const tableOriginal = this.mapOfOriginalTables.get(originalRef);
                //insertHeader
                let theadOriginal = tableOriginal.tHead.cloneNode(true);
                table.insertAdjacentElement("afterbegin", theadOriginal);
            }
        }
    }

    /**
     * Delete thead which are empty inside document
     * @param pages
     */
    afterRendered(pages){
        document.querySelectorAll("tbody").forEach((tbody) =>{
            if(tbody.childNodes.length === 0){
                console.log(tbody.offsetParent);
                tbody.offsetParent.remove();
            }
        });
    }
}

Paged.registerHandlers(BetterTable);