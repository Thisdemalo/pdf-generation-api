# Comment Puppeteer et Pagedjs communiquent-ils ?

## Introduction

Lorsqu'un template html est envoyé au serveur, Puppeteer instancie une nouvelle [Page](https://pptr.dev/#?product=Puppeteer&version=v13.7.0&show=api-class-page). Le template html est placé dans la Page avec la méthode [setContent](https://puppeteer.github.io/puppeteer/docs/puppeteer.page.setcontent/).

Une fois que la page est chargée, c'est là que rentre en jeu [Pagedjs](https://pagedjs.org/). On utilise un script de cette lib qui agit comme un [polyfill](https://pagedjs.org/documentation/2-getting-started-with-paged.js/#:~:text=Using%20Paged.js%20as%20a%20polyfill%20in%20web%20browsers) pour simuler la spec [PagedMedia](https://www.w3.org/TR/css-page-3/) qui n'est pas encore supporté par les navigateurs. Ce script est ajouté à la page.

## Problème ⚠

Avant d'appeler la méthode [pdf](https://github.com/puppeteer/puppeteer/blob/main/docs/api.md#pagepdfoptions) qui génère un pdf à partir du contenu de la Page, il faut attendre que le script de pagedjs "ait fini son travail". En effet, c'est ce dernier qui gère la pagination de la page et qui va la regénérer en incluant la spec Paged Media supporté par Pagedjs ([explication plus précise](https://gitlab.com/math712b/generationpdfapi/-/tree/featPuppeteer#mettre-en-forme-son-html)).

Comment Puppeteer peut savoir quand le script a fini ce qu'il devait faire?

## Comment ce problème est-il traité ? 💡

On veut pouvoir s'abonner à un event du script Pagedjs indiquant que la mise en page est finie. Or le script tourne dans chrome et on veut récupérer l'évènement depuis puppeter.

### Les évènements propagés depuis Pagedjs

La librairie PagedJs implémente un système d'[Hook](https://pagedjs.org/documentation/10-handlers-hooks-and-custom-javascript/) ([qu'est-ce qu'un système d'Hook?](https://fr.wikipedia.org/wiki/Hook_(informatique))). Cela permet de personnaliser le fonctionnement de la librairie en lui faisant réaliser des actions supplémentaires à des moments déterminés. Ce que l'on souhaite dans notre cas, c'est qu'au moment où la mise en page est finie un évènement est créé et propagé pour que Puppeteer sache que le travail de PagedJs est terminé. Du côté de la page où se trouve Puppeteer, elle observe si cet évènement est propagé afin d'attendre de générer le pdf.

Pagedjs émet aussi des événements sur lesquels on peut s'abonner :

- rendered: émis lorsque Pagedjs a fini le rendu de la page

![Diagramme de séquence montrant l'échange entre puppeteer et pagedjs](https://i.ibb.co/V3PVVQr/communication-Pagedjs.png)