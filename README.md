<h1>Introduction</h1>
<p>
Le but de ce service est de pouvoir créer son pdf à partir d'un template html/css qui est envoyé via une requête HTTP.
Le service est dépendant de <a href="https://github.com/pagedjs/pagedjs">pagedjs</a>, qui est une librairie javascript 
qui permet de gérer la pagination, la mise en page avant une impression. En effet, cette dernière permet d'implémenter les 
spécifications <a href="https://www.w3.org/TR/css-page-3/">Paged Media</a> qui ne sont pas encore supportées par les 
navigateurs.
Ces spécifications permettent de rajouter des footer, header, numéros de pages dans des box qui sont placés autour du 
contenu de la page. 
<img src="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/05d0cb51-22ec-4eaf-93c0-222f16de6136/1-image-margin-boxes-large-opt.jpg" alt="">
</p>

<div>
    <h1>Comment la conversion HTML>PDF est faite?</h1>
    <p>
    L'API utilise <a href="https://pptr.dev/">Puppeteer</a> pour générer le PDF. Avant d'envoyer le fichier HTML pour le 
    convertir en PDF, afin que les spécifications <a href="https://www.w3.org/TR/css-page-3/">Paged Media</a> soient 
    prises en compte par Chrome Headless, il est transformé grâce à PagedJS.
    </p>
</div>
<div>
<h1>Mettre en forme son HTML</h1>
<p>
    La balise <code><a href="https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/">@page</a></code>
    permet de mettre en forme son HTML lorsqu'il est transformé en PDF. Grâce à la lib Pagedjs, cette spécification
    est implémentée au contraire des navigateurs qui ne la supportent pas encore.
</p>
<h2>Inclure des numéros de pages</h2>
<p>Avec le code ci-dessous il sera inséré en bas à droite</p>
<pre>
<code>
@page { 
    size: A4;
    margin: 1cm 1.5cm 2cm 1.5cm;
    @bottom-right {
        content: "Page " counter(page) " sur " counter(pages);
        font-size: 10pt;
        padding-bottom: 1cm;
    }
}    
</code>
</pre>
<h2>Insérer des footer, header</h2>
<p>Imaginons que l'on a l'élément HTML ci-dessous que l'on souhaite insérer comme footer au milieu de la page</p>
<p>Il existe pour ça dans la spécification <code>@page</code> les running elements</p>
<pre>
<code>
&lt;div class="footer-service">
    &lt;span style="color: var(--wcs-green); font-weight: bold;">e.SNCF / Direction Design & Développement&lt;/span>
&lt;/div>
</code>
</pre>
<pre>
<code>
@page { 
    size: A4;
    margin: 1cm 1.5cm 2cm 1.5cm;
    @bottom-center {
        content: element(<b style="color: red">footer</b>);
    }
}
.footer-service{
    position: running(<b style="color: red">footer</b>);
}
</code>
</pre>
<img src="doc/pdfFooter.png" alt="Capture d'écran requête postman">
<p>Ainsi, grâce aux running element, on peut insérer des éléments html dans les régions situés autour du contenu du 
document:
</p>
<img src="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/05d0cb51-22ec-4eaf-93c0-222f16de6136/1-image-margin-boxes-large-opt.jpg" alt="">
</div>
<div>
<h1>Endpoints</h1>
<div>
<h2>Générer un pdf</h2>
<code>POST /pdf</code>
<p>
    Le body de la requête est un <a href="https://developer.mozilla.org/fr/docs/Web/API/FormData/FormData">form-data</a> afin de pouvoir envoyer le template html: <br>
</p>
<pre>
<code>curl -k --location --request POST 'https://urlService/pdf' --form 'template=@"cheminFichier.html"' --output /cheminFichier.pdf
</code>
</pre>


</div>
</div>
<div>
    <h1>Exemple</h1>
    <p>
        Afin de tester la génération de pdf, le dossier <a href="example">exemple</a> contient des templates html.
                <img src="doc/exempleReponsePdf.png" alt="Capture d'écran réponse postman contenant un pdf">
    </p>
</div>

